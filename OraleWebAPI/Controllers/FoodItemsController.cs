﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DataAccess.Model.Entities;
using Servicios.DbServices;
using Servicios.Entidades.Salida;
using Servicios.Logica;
using Servicios.Interface;

namespace OraleWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodItemsController : ControllerBase
    {
        private readonly IFoodItemService _foodItemService;


        public FoodItemsController(IFoodItemService foodItemService)
        {
            _foodItemService = foodItemService;
        }

       
        // GET: api/FoodItems
        [HttpGet]
        public async Task<IEnumerable<SFoodItem>> GetFoodItems()
        {   

            return await _foodItemService.ConsultarAsync();
        }

        
    }
}
