using AccesoDatos.Mapper;
using AutoMapper;
using DataAccess.Model.Context;
using DataAccess.Model.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using OraleWebAPI.Entidades;
using Servicios.DbServices;
using Servicios.Interface;
using Servicios.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OraleWebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            // Dependency Injection to DbContext for Azure Web Database Connection
            services.AddDbContext<DBServicioContext>(
                options => options.UseSqlServer(
                    Configuration.GetConnectionString("OraleConnection"),
                    x => x.MigrationsHistoryTable("__EFMigrationsHistory", "OraleDB")
                )
            );

            //Manage Users And Roles

            services.AddDefaultIdentity<ApplicationUser>()
               .AddRoles<ApplicationRole>()
               .AddEntityFrameworkStores<DBServicioContext>();

            // Cors for Angular APP
            services.AddCors();

            //AppSetup Injection
            services.Configure<ApplicationSetup>(Configuration.GetSection("ApplicationSetup"));

            //JSON Web Token Authentication (JWT)

            var key = Encoding.UTF8.GetBytes(Configuration["ApplicationSetup:JWT_Secret"].ToString());

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => {
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });


            //Passwords Managment
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 4;
            }
           );
            // Query services
            //services.AddTransient<IFoodItemsQueryService, FoodItemQueryService>();

            //Mapping

            services.AddAutoMapper(typeof(Startup)); 
            services.AddScoped<IFoodItemService, FoodItemService > ();
            
           
            //var mapperConfig = new MapperConfiguration(m =>
            //{
            //    m.AddProfile(new FoodItemProfile());
            //});


            //IMapper mapper = mapperConfig.CreateMapper();
            //services.AddSingleton(mapper);
            //services.AddSingleton(mapperConfig);
            //services.AddSingleton<IFoodItemService, FoodItemService>();


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "OraleWebAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "OraleWebAPI v1"));
            }

            app.UseCors(builder =>
           builder.WithOrigins(Configuration["ApplicationSetup:Client_URL"].ToString())
           .AllowAnyHeader()
           .AllowAnyMethod()
           );

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
