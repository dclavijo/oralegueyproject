﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OraleWebAPI.Entidades
{
    public class ApplicationSetup
    {
        public string JWT_Secret { get; set; }

        public string Client_URL { get; set; }
    }
}
