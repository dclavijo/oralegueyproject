﻿using Servicios.Entidades.Salida;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interface
{
    public interface IFoodItemService
    {
        Task<IEnumerable<SFoodItem>> ConsultarAsync();
        Task<IEnumerable<SFoodItem>> ConsultarAsyncByID(int id);
    }
}
