﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Entidades.Salida
{
    public class SApplicationRole
    {
        public ICollection<SApplicationUserRole> UserRoles { get; set; }
    }
}
