﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Entidades.Salida
{
    public class SOrderMaster
    {
        
        public long OrderMasterId { get; set; }

        
        public string OrderNumber { get; set; }

        public int CustomerId { get; set; }

        public string PMethod { get; set; }

        public decimal GTotal { get; set; }

        public List<SOrderDetail> OrderDetails { get; set; }

        //[NotMapped]
        //public string DeletedOrderItemIds { get; set; }

    }
}
