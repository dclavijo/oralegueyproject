﻿namespace Servicios.Entidades.Salida
{
    public class SFoodItem
    {

        public int FoodItemId { get; set; }

        public string FoodItemName { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }
    }
}
