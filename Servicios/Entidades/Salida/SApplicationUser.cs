﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Entidades.Salida
{
    public class SApplicationUser
    {
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string BirthDate{ get; set; }

        public string Points { get; set; }

        public string Email { get; set; }
        
        public string PhoneNumber { get; set; }

        public ICollection<SApplicationUserRole> UserRoles { get; set; }
        
    }

}
