﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Entidades.Salida
{
    public class SApplicationUserRole
    {
        public SApplicationRole Role { get; set; }

        public SApplicationUser User { get; set; }
    }
}
