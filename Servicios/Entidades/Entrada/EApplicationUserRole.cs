﻿using DataAccess.Model.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Entidades.Entrada
{
    public class EApplicationUserRole 
    {
        [Required]
        public EApplicationRole Role { get; set; }
        [Required]
        public EApplicationUser User { get; set; }
    }
}
