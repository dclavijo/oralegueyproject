﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Entidades.Entrada
{
    public class EApplicationUser
    {

        [Required, MaxLength(100)]
        public string UserName { get; set; }

        [Required, MaxLength(100)]
        public string FirstName { get; set; }
        
        [Required, MaxLength(100)]
        public string LastName { get; set; }
        
        [Required, MaxLength(100)]
        public string Address { get; set; }
        
        [Required, MaxLength(100)]
        public string BirthDate{ get; set; }
        
        [Required, MaxLength(100)]
        public string Email { get; set; }
        
        [Required, MaxLength(100)]
        public string PhoneNumber { get; set; }

        [Required]
        public string Password { get; set; }

        public string Role { get; set; }



        //public ICollection<EApplicationUserRole> UserRoles { get; set; }

    }

}
