﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Entidades.Entrada
{

    public class EOrderDetail
    {
        [Required]
        public long? OrderDetailId { get; set; }
        [Required]
        public long? OrderMasterId { get; set; }
        [Required]
        public int? FoodItemId { get; set; }
        [Required]
        public decimal? FoodItemPrice { get; set; }
        [Required]
        public int? Quantity { get; set; }

    }
}
