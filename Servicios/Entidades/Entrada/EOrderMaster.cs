﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Entidades.Entrada
{
    public class EOrderMaster
    {
        [Required]       
        public string OrderNumber { get; set; }
        [Required]
        public int? CustomerId { get; set; }
        [Required]
        public string PMethod { get; set; }
        [Required]
        public decimal? GTotal { get; set; }

        //[NotMapped]
        //public string DeletedOrderItemIds { get; set; }

    }
}
