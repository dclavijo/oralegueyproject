﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Entidades.Entrada
{
    public class EApplicationRole
    {
        public ICollection<EApplicationUserRole> UserRoles { get; set; }
    }
}
