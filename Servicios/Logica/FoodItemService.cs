﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Servicios.DbServices;
using Servicios.Entidades.Salida;
using Servicios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Logica

{
        public class FoodItemService : IFoodItemService
        { 
        private readonly DBServicioContext _context;
        private readonly MapperConfiguration _mapperConfiguration;
        private readonly IMapper _mapper;

        public FoodItemService(DBServicioContext context, MapperConfiguration mapperConfiguration, IMapper mapper)
        {
            _context = context;
            _mapperConfiguration = mapperConfiguration;
            _mapper = mapper;

        }

        public async Task<IEnumerable<SFoodItem>> ConsultarAsync()
        {

            var consulta = await _context.FoodItems.ProjectTo<SFoodItem>(_mapperConfiguration).ToListAsync();
            return consulta;
        }
        
        public async Task<IEnumerable<SFoodItem>> ConsultarAsyncByID(int id)
        {

            var foodItem = await _context.FoodItems.FindAsync(id);

            if (foodItem != null)
            {
                var foodItemTable = _mapper.Map<SFoodItem>(foodItem);

                return (IEnumerable<SFoodItem>)foodItemTable;
                
            }
            else
            {
                return null;
            }

        }
        }   


        //public async Task<DataCollection<FoodItemsDTO>> GetAllAsync(int page, int take, IEnumerable<int> FoodItems = null)
        //{
        //    var collection = await _context.FoodItems
        //        .Where(x => FoodItems == null || FoodItems.Contains(x.FoodItemId))
        //        .OrderBy(x => x.FoodItemName)
        //        .GetPagedAsync(page, take);

        //    return collection.MapTo<DataCollection<FoodItemsDTO>>();
        //}

        //public async Task<FoodItemsDTO> GetAsync(int id)
        //{
        //    return (await _context.FoodItems.SingleAsync(x => x.FoodItemId == id)).MapTo<FoodItemsDTO>();
        //}
        
}
