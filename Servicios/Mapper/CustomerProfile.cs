﻿using AutoMapper;
using DataAccess.Model.Entities;
using Servicios.Entidades.Entrada;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Mapper
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            CreateMap<Customer, ECustomer> (); 
            CreateMap<Customer, SCustomer>().ReverseMap();
        }

    }
}
