﻿using AutoMapper;
using DataAccess.Model.Entities;
using Servicios.Entidades.Entrada;
using Servicios.Entidades.Salida;

namespace Servicios.Mapper
{
    public class FoodItemProfile : Profile
    {
        public FoodItemProfile()
        {
            CreateMap<FoodItem, EFoodItem>();
            CreateMap<FoodItem, SFoodItem>().ReverseMap();
        }

    }
}
