﻿using AutoMapper;
using DataAccess.Model.Entities;
using Servicios.Entidades.Entrada;
using Servicios.Entidades.Salida;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Mapper
{
    public class ApplicationUserProfile : Profile
    {
        public ApplicationUserProfile()
        {
            CreateMap<ApplicationUser, EApplicationUser>();
            CreateMap<ApplicationUser, SApplicationUser>().ReverseMap();
            
        }

    }
}
