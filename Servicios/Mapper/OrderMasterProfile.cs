﻿using AutoMapper;
using DataAccess.Model.Entities;
using Servicios.Entidades.Entrada;
using Servicios.Entidades.Salida;

namespace Servicios.Mapper
{
    public class OrderMasterProfile : Profile
    {
        public OrderMasterProfile()
        {
              CreateMap<OrderMaster, EOrderMaster>();
              CreateMap<OrderMaster, SOrderMaster>().ReverseMap();
        }
    }
}
