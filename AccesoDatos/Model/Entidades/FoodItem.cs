﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model.Entities
{
    public class FoodItem
    {
       
        public int FoodItemId { get; set; }

        public string FoodItemName { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }
    }
}
