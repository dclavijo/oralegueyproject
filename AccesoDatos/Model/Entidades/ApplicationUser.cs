﻿using AccesoDatos.Model.Entidades;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model.Entities
{
    public class ApplicationUser : IdentityUser
    {
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string BirthDate{ get; set; }
        public string Points { get; set; }

        public ICollection<ApplicationUserRole> UserRoles { get; set; }
        
    }

}
