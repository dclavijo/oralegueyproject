﻿using DataAccess.Model.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Modelo.Configuration
{
    public class CustomerConfiguration
    {

        public CustomerConfiguration(EntityTypeBuilder<Customer> entityBuilder)
        {
            entityBuilder.HasKey("CustomerID");
            entityBuilder.HasIndex(e => e.CustomerID);
            entityBuilder.Property(e => e.CustomerName).IsRequired().HasMaxLength(100);
            entityBuilder.Property(e => e.Address).IsRequired().HasMaxLength(100);
            entityBuilder.Property(e => e.Phone).IsRequired().HasMaxLength(20);


        }


    }
}
