﻿using DataAccess.Model.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Model.Configuration
{
    public class OrderMasterConfiguration
    {
        public OrderMasterConfiguration(EntityTypeBuilder<OrderMaster> entityBuilder)
        {
            entityBuilder.HasIndex(e => e.OrderMasterId);
            entityBuilder.Property(e =>e.GTotal).HasPrecision(8, 2);


        }

    }
}
