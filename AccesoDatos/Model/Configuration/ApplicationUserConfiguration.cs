﻿using DataAccess.Model.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Model.Configuration
{
    public class ApplicationUserConfiguration
    {
        public ApplicationUserConfiguration(EntityTypeBuilder<ApplicationUser> entityBuilder)
        {
            entityBuilder.HasIndex(e => e.Id);
            entityBuilder.Property(e => e.FirstName).IsRequired().HasMaxLength(100);
            entityBuilder.Property(e => e.LastName).IsRequired().HasMaxLength(100);
            entityBuilder.Property(e => e.Address).IsRequired().HasMaxLength(100);
            entityBuilder.Property(e => e.Email).IsRequired().HasMaxLength(100);
            entityBuilder.Property(e => e.PhoneNumber).IsRequired().HasMaxLength(20);
                      
        }
    }
}
