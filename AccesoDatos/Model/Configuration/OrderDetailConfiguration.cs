﻿using DataAccess.Model.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Model.Configuration
{
    public class OrderDetailConfiguration
    {
        public OrderDetailConfiguration(EntityTypeBuilder<OrderDetail> entityBuilder)
        {

            entityBuilder.HasKey(e => e.OrderDetailId);
            entityBuilder.HasIndex(e => e.OrderDetailId);
            entityBuilder.Property("FoodItemPrice").HasPrecision(8,2);

        }
    }
}
