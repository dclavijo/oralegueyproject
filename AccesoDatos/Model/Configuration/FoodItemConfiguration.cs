﻿using DataAccess.Model.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Model.Configuration
{
    public class FoodItemConfiguration
    {
        public FoodItemConfiguration(EntityTypeBuilder<FoodItem> entityBuilder)
        {   //TODO Asking About Migrations
            entityBuilder.HasIndex(e => e.FoodItemId);
            //entityBuilder.Property(e => e.FoodItemName).IsRequired().HasMaxLength(100);
            entityBuilder.Property(e => e.Price).HasPrecision(8, 2);


        }
    }
}
