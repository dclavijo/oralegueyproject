﻿using AccesoDatos.Model.Configuration;
using AccesoDatos.Model.Entidades;
using AccesoDatos.Modelo.Configuration;
using DataAccess.Model.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model.Context
{
    public class AuthContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public AuthContext(DbContextOptions<AuthContext> options) : base(options)
        {
        }

        public AuthContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<ApplicationUser> AplicationUsers { get; set; }
        public DbSet<ApplicationRole> AplicationRoles { get; set; }
        public DbSet<ApplicationUserRole> AplicationUserRoles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Customer> Employee { get; set; }
        public DbSet<FoodItem> FoodItems { get; set; }
        public DbSet<OrderMaster> OrderMasters { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Database schema
            builder.HasDefaultSchema("OraleDB");

            base.OnModelCreating(builder);

            //Model Contraints
            ModelConfig(builder);

            //TODO .hasrestantes -- required y columns

            //base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<OrderDetail>()
            //    .HasKey(b => b.OrderDetailId);
            
            //modelBuilder.Entity<Customer>()
            //    .HasKey(b => b.CustomerID);
            
            //modelBuilder.Entity<FoodItem>()
            //    .HasKey(b => b.FoodItemId);
            
            //modelBuilder.Entity<OrderMaster>()
            //    .HasKey(b => b.OrderMasterId);
        }

        private void ModelConfig(ModelBuilder modelBuilder)
        {
            new CustomerConfiguration(modelBuilder.Entity<Customer>());
            new FoodItemConfiguration(modelBuilder.Entity<FoodItem>());
            new OrderDetailConfiguration(modelBuilder.Entity<OrderDetail>());
            new OrderMasterConfiguration(modelBuilder.Entity<OrderMaster>());
        }

    }
    


}
